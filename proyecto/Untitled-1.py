#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class WNMensaje():
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("./objetos.ui")

        self.mensaje = builder.get_object("mensaje")


        self.mensaje.show_all()