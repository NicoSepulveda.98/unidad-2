#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class WNMensaje():
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("./ventanap.ui")

        self.mensaje = builder.get_object("mensaje")
        self.acept = builder.get_object("aceptar")

        self.lbl_changed = builder.get_object("lbl_changed") 
        self.lbl = builder.get_object("lbl_n")

        # ListStore
        self.store = builder.get_object("ListStore")
        self.store.connect("cursor-changed", self.store_changed)
        self.model = Gtk.ListStore(*(2 * [str]))
        self.store.set_model(model=self.model)
        cell  = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn(title="Nombre",
                                    cell_renderer=cell,
                                    text=1)
        self.store.append_column(column)
        self.model.append(["Puto","Proyecto"])

    def store_changed(self, tree=None):
        model, it = self.store.get_selection().get_selected()
        if model is None or it is None:
            return

        self.lbl_changed.set_text("Puto Proyecto ")


        self.mensaje.show_all()